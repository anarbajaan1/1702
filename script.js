const users = [{
    _id: "5cdce6ce338171bb473d2855",
    index: 0,
    isActive: false,
    balance: 2397.64,
    age: 39,
    name: "Lucile Finley",
    gender: "female",
    company: "ZOXY",
    email: "lucilefinley@zoxy.com",
    phone: "+1 (842) 566-3328",
    registered: "2015-07-12T09:39:03 -03:00"
},
{
    _id: "5cdce6ce0aa8d071fa4f4cc5",
    index: 1,
    isActive: true,
    balance: 2608.48,
    age: 33,
    name: "Woodward Grimes",
    gender: "male",
    company: "FORTEAN",
    email: "woodwardgrimes@fortean.com",
    phone: "+1 (960) 436-3138",
    registered: "2014-09-08T03:24:39 -03:00"
},
{
    _id: "5cdce6ce103de120d32d6fe4",
    index: 2,
    isActive: true,
    balance: 1699.99,
    age: 25,
    name: "Robinson Coleman",
    gender: "male",
    company: "GENMOM",
    email: "robinsoncoleman@genmom.com",
    phone: "+1 (852) 543-3171",
    registered: "2019-04-23T08:24:58 -03:00"
},
{
    _id: "5cdce6cebada7a418d8ccb3d",
    index: 3,
    isActive: true,
    balance: 2621.84,
    age: 25,
    name: "Austin Benton",
    gender: "male",
    company: "ZILIDIUM",
    email: "austinbenton@zilidium.com",
    phone: "+1 (977) 573-2627",
    registered: "2016-08-02T10:08:24 -03:00"
},
{
    _id: "5cdce6ced81fe99596d9cef5",
    index: 4,
    isActive: true,
    balance: 1297.31,
    age: 37,
    name: "Casandra Stout",
    gender: "female",
    company: "ANACHO",
    email: "casandrastout@anacho.com",
    phone: "+1 (929) 465-3804",
    registered: "2018-04-14T11:27:26 -03:00"
},
{
    _id: "123",
    index: 4,
    isActive: true,
    balance: 1297.31,
    age: 37,
    name: "Casandra Stout",
    gender: "female",
    company: "ANACHO",
    email: "casandrastout@anacho.com",
    phone: "+1 (929) 465-3804",
    registered: "2018-04-14T11:27:26 -03:00"
},
{
    _id: "5cdce6ce6c3ae6c4d6f39e88",
    index: 5,
    isActive: false,
    balance: 2165.49,
    age: 20,
    name: "Valencia Carrillo",
    gender: "male",
    company: "XEREX",
    email: "valenciacarrillo@xerex.com",
    phone: "+1 (977) 522-3378",
    registered: "2014-02-14T11:45:27 -02:00"
}
];


//Перебирающие методы массивов
// forEach, map, filter, reduce, sort, every, some

// item, index, arr
// users.forEach(function (item, index, arr) {
//     // console.log(item, index, arr)
//     item.balance += 1000000;
// })

// console.log(users)


//Стрелочные функции

// const test = (a, ...args) => {
//     console.log(a, args)
//     return args[0]
// }

// test(100, 10, 2, 34, -7)

// const product={
//     name:'Iphone',
//     getName:() => {
//         console.log(this)
//     }
// }

// product.getName()

// Стрелочные функции внутри объектов не сохраняют this

// const sum = (arr, props) => {
//     let sum = 0
//     for (let user of arr) {
//         sum += user.balance
//         // sum += user[props]
//     }
//     console.log(sum)
//     return sum
// }

// sum(users)




// const sum = (arr) => {
//     let sum = 0
//     for(let user of arr){
//         sum += user.age
//     }
//     console.log(sum/users.length)
//     return sum / users.length

// }

// sum(users)


// let sum = (arr) => {
//     let max = arr[0]
//     for(let item of arr){
//         if(item.age > max.age){
//             max = item
//         }
//     }
//     console.log(max.age)
//     return max.age
// }

// sum(users)

// let sum1 = (arr) => {
//     let min = arr[0]
//     for(let item of arr){
//         if(item.age < min.age){
//             min = item
//         }
//     }
//     console.log(min.age)
//     return min.age
// }

// sum1(users)